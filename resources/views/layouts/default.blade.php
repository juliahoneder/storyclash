<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    @include('includes.head')
</head>
<body>
<div class="container">
    <div id="main" class="row">
        @yield('content')
    </div>
</div>

<script type="text/javascript" src="{{ secure_asset('js/main.js') }}"></script>

</body>
</html>
