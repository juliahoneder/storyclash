@extends('layouts.default')

@section('content')

    <div class="sidebar">
        <a class="active" id="sidebar-show" href="#">
            <img src="/img/favicon.png" alt="Storyclash" class="logo-img loading" data-was-processed="true">
        </a>
    </div>

    <div class="reports-subnav" id="sidebar-panel">
        <a class="btn btn-reports-subnav" id="sidebar-hide">
            <i class="fa-solid fa-angle-left"></i>
            <i class="fa-solid fa-bookmark"></i>
            Saved Reports
        </a>

        <button class="btn btn-reports-collection" id="accordion">
            My Reports
            <i class="fa fa-angle-down"></i>
        </button>

        <div class="reports-collection-content" id="panel">
            @foreach ($reports as $report)
                <div class="report-collection-item">
                    <div class="report-name">
                        <i class="fa-solid fa-{{ $report->icon }}"></i>

                        <form method="POST"
                              action="{{ route('reports.update', $report->id) }}"
                              class="report-name-form hide">
                            @csrf
                            @method('PUT')

                            <input type="submit" hidden>
                            <input type="text" id="name" name="name" class="report-name-form-input" value="{{ $report->name }}" required>
                        </form>

                        <span class="report-name-text">{{ $report->name }}</span>
                    </div>

                    <div class="report-menu">
                        <button class="btn btn-report-menu-button">
                            <i class="fa-solid fa-ellipsis-vertical"></i>
                        </button>
                        <div class="report-menu-content">
                            <button type="submit" class="btn btn-rename-report report-menu-item">
                                <i class="fa-solid fa-pen"></i>
                                Rename
                            </button>
                            <form method="POST"
                                  action="{{ route('reports.destroy', $report->id) }}"
                                  class="delete-report-form">
                                @csrf
                                @method('DELETE')

                                <button type="submit" class="btn btn-delete-report report-menu-item">
                                    <i class="fa-solid fa-trash"></i>
                                    Delete
                                </button>
                            </form>
                        </div>
                    </div>
                </div>
            @endforeach

            <div class="report-collection-item hide" id="new-report-collection-item">
                <div class="report-name">
                    <form method="POST"
                          action="{{ route('reports.store') }}"
                          class="new-report-collection-item-form">
                        @csrf
                        @method('POST')

                        <i class="fa-solid fa-file"></i>
                        <input type="submit" hidden>
                        <input type="text" id="icon" name="icon" value="file" hidden>
                        <input type="text" id="name" name="name" class="new-report-collection-item-form-input" required>
                    </form>
                </div>
            </div>

            <button type="submit" class="btn btn btn-save-report" id="btn-new-report">
                <i class="fa-solid fa-circle-plus"></i>
                Save Report
            </button>
        </div>
    </div>

    <div class="content">
    </div>

@stop
