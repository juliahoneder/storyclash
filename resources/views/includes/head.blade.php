<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="Storyclash | Report Management">

<link rel="icon" type="image/png" href="/img/favicon.png">
<link rel="stylesheet" type="text/css" href="{{ secure_asset('css/main.css') }}">

<title>Storyclash</title>

<script src="https://kit.fontawesome.com/1ef6e600b4.js" crossorigin="anonymous"></script>
