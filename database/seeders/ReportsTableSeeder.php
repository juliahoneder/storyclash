<?php

namespace Database\Seeders;

use DB;
use App\Models\Report;
use Illuminate\Database\Seeder;

class ReportsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $reports = [
            [
                'name' => 'Mercedes today',
                'icon' => 'copyright'
            ],
            [
                'name' =>  'Mercedes competitors',
                'icon' => 'eye'
            ],
            [
                'name' => 'Mercedes influencer',
                'icon' => 'trophy'
            ]
        ];

        foreach ($reports as $report) {
            $newReport = new Report();

            $newReport->name = $report['name'];
            $newReport->icon = $report['icon'];

            $newReport->save();
        }
    }
}
