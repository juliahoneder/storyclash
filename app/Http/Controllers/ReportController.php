<?php

namespace App\Http\Controllers;

use App\Models\Report;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ReportController extends Controller
{
    public function index()
    {
        $reports = Report::all();
        return view('pages.report', compact('reports'));
    }

    public function store(Request $request): RedirectResponse
    {
        DB::beginTransaction();

        try {
            $createFields = [
                'name' => $request->get('name'),
                'icon' => 'file'
            ];

            Report::create($createFields);

            DB::commit();

            return redirect()
                ->route('reports.index')
                ->with('error', 'Successfully inserted report.');

        } catch (\Exception $e) {
            DB::rollBack();

            return redirect()
                ->route('reports.index')
                ->with('error', 'Failed to insert report.');
        }
    }

    public function update(Request $request, $id): RedirectResponse
    {
        DB::beginTransaction();

        try {
            $updateFields = [
                'name' => $request->get('name')
            ];

            $report = Report::where('id', $id)->first();
            $report->update($updateFields);
            $report->save();

            DB::commit();

            return redirect()
                ->route('reports.index')
                ->with('error', 'Successfully updated report.');

        } catch (\Exception $e) {
            DB::rollBack();

            return redirect()
                ->route('reports.index')
                ->with('error', 'Failed to update report.');
        }
    }

    public function destroy($id): RedirectResponse
    {
        try {
            Report::where('id', $id)->delete();

            return redirect()
                ->route('reports.index')
                ->with('error', 'Successfully deleted report.');

        } catch (\Exception $e) {
            return redirect()
                ->route('reports.index')
                ->with('error', 'Failed to delete report.');
        }
    }
}
