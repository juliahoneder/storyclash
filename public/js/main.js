// --------------------------------------------------------------------------------------------------------------------
// sidebar
// --------------------------------------------------------------------------------------------------------------------
let sidebarBtnShow = document.getElementById('sidebar-show');
let sidebarBtnHide = document.getElementById('sidebar-hide');
let sidebarContent = document.getElementById('sidebar-panel');

sidebarBtnShow.onclick = () => {
    sidebarContent.classList.remove('hide');
}

sidebarBtnHide.onclick = () => {
    sidebarContent.classList.add('hide');
}

// --------------------------------------------------------------------------------------------------------------------
// accordion
// --------------------------------------------------------------------------------------------------------------------
let accBtn = document.getElementById('accordion');
let accContent = document.getElementById('panel');

accBtn.onclick = () => {
    if (accContent.classList.contains('hide')) {
        accContent.classList.remove('hide');

        addIconAngle(accBtn, 'down');
        accBtn.getElementsByClassName('fa-angle-left')[0].remove();
    } else {
        accContent.classList.add('hide');

        addIconAngle(accBtn, 'left');
        accBtn.getElementsByClassName('fa-angle-down')[0].remove();
    }
}

// --------------------------------------------------------------------------------------------------------------------
// report entry menu
// --------------------------------------------------------------------------------------------------------------------
// report entry menu | open
let reportMenuBtns = document.getElementsByClassName('btn-report-menu-button');

let openReportMenu = (event) => {
    event.stopPropagation();

    for (let i = 0; i < reportMenuContent.length; i++) {
        reportMenuContent[i].classList.remove('show');
    }

    let parentNode = event.target.parentNode.parentNode;
    let menu = parentNode.querySelector('.report-menu-content');
    menu.classList.add('show');
}

for (let i = 0; i < reportMenuBtns.length; i++) {
    reportMenuBtns[i].addEventListener('click', openReportMenu, false);
}

// report entry menu | close
let closeReportMenu = (event) => {
    for (let i = 0; i < reportMenuContent.length; i++) {
        if (!reportMenuContent[i].contains(event.target)) {
            reportMenuContent[i].classList.remove('show');
        }
    }
}

document.addEventListener('click', closeReportMenu, false);

// --------------------------------------------------------------------------------------------------------------------
// save report
// --------------------------------------------------------------------------------------------------------------------
let newReportBtn = document.getElementById('btn-new-report');
let newReportCollectionItem = document.getElementById('new-report-collection-item');
let  newReportCollectionItemFormInput = document.getElementsByClassName('new-report-collection-item-form-input');

// save report | show
newReportBtn.onclick = () => {
    newReportCollectionItem.classList.remove('hide');
    newReportCollectionItemFormInput[0].focus();
}

// save report | hide
let hideNewReportInput = (event) => {
    if (event.keyCode == 27) {
        newReportCollectionItem.classList.add('hide');
        newReportCollectionItemFormInput[0].value = '';
    }
}

document.addEventListener('keydown', hideNewReportInput, false);

// --------------------------------------------------------------------------------------------------------------------
// rename report
// --------------------------------------------------------------------------------------------------------------------
let renameReportBtn = document.getElementsByClassName('btn-rename-report');
let renameReportNameForm = document.getElementsByClassName('report-name-form');
let renameReportNameFormInput = document.getElementsByClassName('report-name-form-input');
let reportMenuContent = document.getElementsByClassName('report-menu-content');
let reportNameText = document.getElementsByClassName('report-name-text');

// rename report | show
for (let i = 0; i < renameReportBtn.length; i++) {
    renameReportBtn[i].onclick = () => {
        renameReportNameForm[i].classList.remove('hide');
        reportMenuContent[i].classList.remove('show');
        reportNameText[i].classList.add('hide');
        renameReportNameFormInput[i].focus();
    }
}

// rename report | hide
let hideRenameReportForm = (event) => {
    for (let i = 0; i < reportMenuContent.length; i++) {
        if (event.keyCode == 27) {
            renameReportNameForm[i].classList.add('hide');
            reportNameText[i].classList.remove('hide');
            renameReportNameFormInput[i].value = reportNameText[i].innerText;
        }
    }
}

document.addEventListener('keydown', hideRenameReportForm, false);

// --------------------------------------------------------------------------------------------------------------------
// helper functions
// --------------------------------------------------------------------------------------------------------------------
let addIconAngle = (element, direction) => {
    let icon = document.createElement('i');
    icon.className = `fa fa-angle-${direction}`;
    element.appendChild(icon);
}
